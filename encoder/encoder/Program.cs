﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace encoder
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(@"Author: Paweł Paczuski p.paczuski@stud.elka.pw.edu.pl
This program is used to help with encoding numbers that are used as input for adder that was designed as a challenge during my Operating Systems course. 


");
            int a, b;
            do
            {
                Console.WriteLine("First 8-bit number base 10");
            }
            while (!int.TryParse(Console.ReadLine(), out a));

            do
            {
                Console.WriteLine("Second 8-bit number base 10");
            }
            while (!int.TryParse(Console.ReadLine(), out b));

            string aBin = toBin(a);
            string bBin = toBin(b);
            int sum = a + b;
            string sumBin = toBin(sum);

            Console.WriteLine($"Representation of the numbers {a} and {b} in binary: \n {a}={aBin}, {b}={bBin}, their sum {sum}={sumBin}");
            Console.WriteLine("Connectors layout:");
            Console.WriteLine(connectorsLayout(aBin, bBin));
            Console.WriteLine("Diodes should have the following layout:");
            Console.WriteLine(resultsLayout(sum));
        }
        static string toBin(int num)
        {
            string numBin = Convert.ToString(num, 2);
            int paddingCount = 8 - numBin.Length;
            return $"{ new StringBuilder(paddingCount).Insert(0, "0", paddingCount).ToString()}{numBin}";
        }
        static string negate(char c)
        {
            return c == '0' ? "1" : "0";
        }
        static string connectorsLayout(string num1, string num2)
        {
            num1 = reverse(num1);
            num2 = reverse(num2);
            return $"{num1[3]} {num2[3]} \t {num1[2]} {num2[2]} \t\t {num1[7]} {num2[7]} \t {num1[6]} {num2[6]}\n{negate(num1[0])} {negate(num2[0])} \t {negate(num1[1])} {negate(num2[1])} \t\t {negate(num1[4])} {negate(num2[4])} \t {negate(num1[5])} {negate(num2[5])}";
        }
        static string reverse(string str)
        {
            return string.Join("", str.Reverse());
        }
        static string resultsLayout(int num)
        {
            var res = toBin(num).Reverse().ToList();
            res.Insert(4, '\n');
            return string.Join("", res);
        }
    }
}
